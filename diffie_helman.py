import random

# Étape 1 : Paramètres publics
p = 23  # Un nombre premier
g = 5   # Un générateur

# Étape 2 : Alice choisit un nombre secret a
a = random.randint(1, p - 1)

# Étape 2 : Bob choisit un nombre secret b
b = random.randint(1, p - 1)

# Étape 3 : Alice calcule A et l'envoie à Bob
A = #TODO 

# Étape 4 : Bob calcule B et l'envoie à Alice
B = #TODO 

# Étape 5 : Alice calcule la clé partagée
s_alice = #TODO 

# Étape 5 : Bob calcule la clé partagée
s_bob = #TODO 

# Vérification : Les deux devraient avoir la même clé partagée
if s_alice == s_bob:
	print("Clé partagée : ", s_alice)
else:
	print("Erreur : Les clés partagées ne correspondent pas.")

